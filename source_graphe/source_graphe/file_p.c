#include <stdlib.h>

#include "graphe.h"
#include "file_p.h"

pfile_p_t creer_file_p ()
{
  pfile_p_t pointeur = malloc(sizeof(file_p_t));
 
  pointeur->highest_p= 0;

  return pointeur;
}

int detruire_file_p (pfile_p_t f)
{
  free(f);
  return 0 ;
}


int file_vide_p (pfile_p_t f)
{
   if (f->Tab[0] == NULL)
      return 1;
   return 0;
}

int file_pleine_p (pfile_p_t f)
  {
   if (f->Tab[99] != NULL)
      return 1;
   return 0;
}

psommet_t defiler_p (pfile_p_t f)
{
    psommet_t elementDefile;
    psommet_t toReturn;

    int i = 0;
    int min = 1000;
    int index = 0;
    while (f->tab[i] != NULL)
    {
      if (f->tab[i]->priority < min){
        min = f->tab[i]->priority;
        index = i;
      }
    }
    elementDefile = f->tab[index];
    toReturn = elementDefile->stock;
    if (f->tab[index+1] == NULL){
      f->tab[index] == NULL;
    }
    else{
     while (f->tab[index] != NULL)
      {
        f->tab[index] = f->tab[index+1];
        index++;
      }
    }
    elementDefile = f->tab[index];
    return toreturn;
}

int enfiler_p (pfile_p_t f, psommet_t p, int priority)
{
  pitem_t toStock;
  toStock.stock = p;
  toStock.priority = priority;
  int i = 0;
  while (f->tab[i] != NULL)
  {
    i++;
  }
  f->tab[i] = toStock;
}

void change_p (pfile_p_t f, int label, int newPriority)
{
  pitem_t toStock;
  int i = 0;
  while (f->tab[i] != NULL)
  {
    if(f->tab[i]->stock->label == label)
      f->tab[i]->priority = newPriority;

    i++;
  }
}