#include <stdio.h>
#include <stdlib.h>

#include "file_p.h"

#include "graphe.h"


int main (int argc, char **argv)
{
  pgraphe_t g ;
  pchemin_t c = (pchemin_t) malloc (sizeof(chemin_t)) ;
  int nc ;
  
  if (argc != 2)
    {
      fprintf (stderr, "erreur parametre \n") ;
      exit (-1) ;
    }

  /*
    la fonction lire_graphe alloue le graphe (matrice,...) 
    et lit les donnees du fichier passe en parametre
  */
  
  
  lire_graphe (argv [1], &g) ;

  /*
    la fonction ecrire_graphe affiche le graphe a l'ecran
  */  
  
  printf ("nombre de sommets du graphe %d nombre arcs %d \n", nombre_sommets (g), nombre_arcs (g)) ;
  fflush (stdout) ;
  
  ecrire_graphe (g) ;      

  nc = colorier_graphe (g) ;
  
  printf ("nombre chromatique graphe = %d\n", nc) ;

  ecrire_graphe_colorie (g) ;

  psommet_t tmp = chercher_sommet (g, 1);
  while(tmp !=NULL){
    tmp->couleur = 1;
    tmp = tmp->sommet_suivant;
  }
  printf ("-----------------TESTS SUR LES PARCOURS-------------------- \n") ;

  printf ("-----------------Test afficher_graphe_largeur-------------------- \n") ;

  afficher_graphe_largeur (g, 3);

  printf ("-----------------Test afficher_graphe_profondeur----------------- \n") ;

  tmp = chercher_sommet (g, 1);
  while(tmp !=NULL){
    tmp->couleur = 1;
    tmp = tmp->sommet_suivant;
  }

  afficher_graphe_profondeur (g, 3);

  printf ("----------------------Test algo_dijkstra------------------------- \n") ;

  algo_dijkstra (g, 3);

  psommet_t Init = chercher_sommet (g, 1);
  c->debut = Init;
  c->arc_actuel = Init->liste_arcs;
  c->fin = Init->liste_arcs->dest;
  c->chemin_suivant = (pchemin_t) malloc (sizeof(chemin_t)) ;
  Init = Init->sommet_suivant;
  pchemin_t stock = c;
  pchemin_t stock2 = c;
  c = c->chemin_suivant;
  while(Init != NULL){
      c->debut = Init;
      c->arc_actuel = Init->liste_arcs;
      c->fin = Init->liste_arcs->dest;
      c->chemin_suivant = (pchemin_t) malloc (sizeof(chemin_t)) ;
      c = c->chemin_suivant;
      Init = Init->sommet_suivant;
  }

  int res =0;

  printf ("-----------------TESTS SUR LES CHEMINS-------------------- \n") ;

  printf ("-----------------Test elementaire-------------------- \n") ;

  res = elementaire(g, stock);
  if(res == 0)
    printf("le chemin n'est pas élémentaire \n");
  else if (res==1)
    printf("le chemin est élémentaire \n");
  
  stock = stock2;
  
  printf ("-----------------Test simple-------------------- \n") ;

  res = simple(g, stock);
  if(res == 0)
    printf("le chemin n'est pas simple \n");
  else if (res==1)
    printf("le chemin est simple \n");

  stock = stock2;

  printf ("-----------------Test eulerien-------------------- \n") ;

  res = eulerien(g, stock);
  if(res == 0)
    printf("le chemin n'est pas eulerien \n");
  else if (res==1)
    printf("le chemin est eulerien \n");

  stock = stock2;

  printf ("-----------------Test hamiltonien-------------------- \n") ;

  res = hamiltonien(g, stock);
  if(res == 0)
    printf("le chemin n'est pas hamiltonien \n");
  else if (res==1)
    printf("le chemin est hamiltonien \n");



  printf ("-----------------Test graphe_eulerien-------------------- \n") ;

  res = graphe_eulerien(g);
  if(res == 0)
    printf("le graphe n'est pas eulerien \n");
  else if (res==1)
    printf("le graphe est eulerien \n");

  printf ("-----------------Test graphe_hamiltonien-------------------- \n") ;

  res = graphe_hamiltonien(g);
  if(res == 0)
    printf("le graphe n'est pas hamiltonien \n");
  else if (res==1)
    printf("le graphe est hamiltonien \n");

  printf ("-----------------Test distance-------------------- \n") ;

  printf ("-----------------Test excentricite-------------------- \n") ;

  printf ("-----------------Test diametre-------------------- \n") ;

}
