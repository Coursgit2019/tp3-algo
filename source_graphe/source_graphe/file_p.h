
#ifndef PRIORITY
#define PRIORITY

#define MAX_FILE_SIZE_p       100

#include "graphe.h"

typedef struct i{
    psommet_t stock;
    int priority;
}item_t, *pitem_t ;

typedef struct f{
  int highest_p ;
  pitem_t Tab [MAX_FILE_SIZE_p] ;
} file_p_t, *pfile_p_t ;


pfile_p_t creer_file_p () ;

int detruire_file_p (pfile_p_t f) ; 

int file_vide_p (pfile_p_t f) ;

int file_pleine_p (pfile_p_t f) ;

psommet_t defiler_p (pfile_p_t f)  ;

int enfiler_p (pfile_p_t f, psommet_t p, int priority);

void change_p (pfile_p_t f, int label, int newPriority);

#endif