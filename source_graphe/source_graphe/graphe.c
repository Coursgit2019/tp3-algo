/*
  Structures de type graphe
  Structures de donnees de type liste
  (Pas de contrainte sur le nombre de noeuds des  graphes)
*/


#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "graphe.h"
#include "file_p.h"

#include "abr.h"
#include "file.h"


psommet_t chercher_sommet (pgraphe_t g, int label)
{
  psommet_t s ;

  s = g ;

  while ((s!= NULL) && (s->label != label))
    {
      s = s->sommet_suivant ;
    }
  return s ;
}

parc_t existence_arc (parc_t l, psommet_t s)
{
  parc_t p = l ;

  while (p != NULL)
    {
      if (p->dest == s)
	return p ;
      p = p->arc_suivant ;
    }
  return p ;
  
}


void ajouter_arc (psommet_t o, psommet_t d, int distance)
{
  parc_t parc ;

  parc = (parc_t) malloc (sizeof(arc_t)) ;

  if (existence_arc (o->liste_arcs, d) != NULL)
    {
      fprintf(stderr, "ajout d'un arc deja existant\n") ;
      exit (-1) ;
    }
  
  parc->poids = distance ;
  parc->dest = d ;
  parc->arc_suivant = o->liste_arcs ;
  o->liste_arcs = parc ;
  return ;
}



// ===================================================================

int nombre_sommets (pgraphe_t g)
{
  psommet_t p = g ;
  int nb = 0 ;

  while (p != NULL)
    {
      nb = nb + 1 ;
      p = p->sommet_suivant ;
    }

  return nb ;
}

int nombre_arcs (pgraphe_t g)
{

  psommet_t p = g ;
  int nb_arcs = 0 ;

  while (p != NULL)
    {
      parc_t l = p->liste_arcs ;

      while (l != NULL)
	{
          nb_arcs = nb_arcs + 1 ;
	  l = l->arc_suivant ;
	}
      
      p = p->sommet_suivant ;
    }
  return nb_arcs ;
}

void init_couleur_sommet (pgraphe_t g)
{
  psommet_t p = g ;

  while (p != NULL)
    {
      p->couleur = 0 ; // couleur indefinie
      p = p->sommet_suivant ; // passer au sommet suivant dans le graphe
    }
  
  return ;
}

int colorier_graphe (pgraphe_t g)
{
  /*
    coloriage du graphe g
    
    datasets
    graphe data/gr_planning
    graphe data/gr_sched1
    graphe data/gr_sched2
  */

  psommet_t p = g;
  parc_t a;
  int couleur;
  int max_couleur = INT_MIN; // -INFINI

  int change;

  init_couleur_sommet(g);

  while (p != NULL) {
    couleur = 1; // 1 est la premiere couleur

    // Pour chaque sommet, on essaie de lui affecter la plus petite couleur

    // Choix de la couleur pour le sommet p

    do {
      a = p -> liste_arcs;
      change = 0;

      while (a != NULL) {
        if (a -> dest -> couleur == couleur) {
          couleur = couleur + 1;
          change = 1;
        }
        a = a -> arc_suivant;
      }

    } while (change == 1);

    // couleur du sommet est differente des couleurs de tous les voisins

    p -> couleur = couleur;
    if (couleur > max_couleur)
      max_couleur = couleur;

    p = p -> sommet_suivant;
  }

  return max_couleur;
}

void afficher_graphe_largeur (pgraphe_t g, int r)
{
  /*
    afficher les sommets du graphe avec un parcours en largeur
  */
  psommet_t toDisplay = chercher_sommet (g, r);
  toDisplay->couleur = 2;
  pfile_t f=creer_file();
  enfiler(f,toDisplay);
  while(!(file_vide(f))){
    toDisplay = defiler(f);
    printf (" %d \n", toDisplay->label);
    parc_t toRoam = toDisplay->liste_arcs;
    while(toRoam != NULL){
      if(toRoam->dest->couleur != 2){
          toRoam->dest->couleur = 2;
          enfiler(f, toRoam->dest);
      }  
      toRoam = toRoam->arc_suivant;
  }
}
  return ;
}


void afficher_graphe_profondeur (pgraphe_t g, int r)
{
  /*
    afficher les sommets du graphe avec un parcours en profondeur
  */
  psommet_t tmp = chercher_sommet (g, r);
  if(tmp->couleur!=2){
      tmp->couleur = 2;
      printf (" %d \n", tmp->label);
  }
  parc_t toRoam = tmp->liste_arcs;
  while(toRoam != NULL){
      if(toRoam->dest->couleur != 2){
          toRoam->dest->couleur = 2;
          printf (" %d \n", toRoam->dest->label);
      }  
      toRoam = toRoam->arc_suivant;
  }
  if(tmp->sommet_suivant != NULL){
      tmp = tmp->sommet_suivant;
      afficher_graphe_profondeur (g, tmp->label);
  }

  return ;
}

void algo_dijkstra (pgraphe_t g, int r)
{
  /*
    Algorithme de dijkstra
    des variables ou des chanmps doivent etre ajoutees dans les structures.
  */
  //Initialisation
  int longueur = nombre_sommets(g);
  pfile_p_t f=creer_file_p();
  psommet_t toIterOn = chercher_sommet(g,1);
  while(toIterOn != NULL){
    enfiler_p(f, toIterOn, 1000);
    toIterOn = toIterOn->sommet_suivant;
  }
  int min = 1000;
  g->distance = (int*) malloc (sizeof(int) * longueur) ;
  g->parent = (int*) malloc (sizeof(int) * longueur) ;
  change_p (f, r, 0);
  toIterOn = chercher_sommet(g,r);
  parc_t arc;
  //Déroulement
  while(f != NULL){
    toIterOn = defiler_p(f);
    arc = toIterOn->liste_arcs;
    while(arc != NULL){
      relachement(g, toIterOn->label, arc->dest->label, arc->poids);
      arc = arc->arc_suivant;
    }

  }
  afficher_resultat_dijkstra(g);
  return ;
}

void relachement (pgraphe_t g, int u, int v, int poids, pfile_p_t f)
{
  int actuel;
  int fin = 0;
  int debut = 0;
  int i = 0;
  while (f->Tab[i] != NULL)
    {
      if (f->Tab[i]->stock->label == u){
        actuel = f->Tab[i]->priority;
      }
      if (f->Tab[i]->stock->label == v){
        fin = f->Tab[i]->priority;
      }
      i++;
    }
  if(fin>(debut+poids))
  {
    change_p (f, v, debut + poids);
    g->parent[v] = u;
  }
  return ;

}

void afficher_resultat_dijkstra (pgraphe_t g)
{
  /*
    algorithme de dijkstra
    des variables ou des chanmps doivent etre ajoutees dans les structures.
  */
  psommet_t toIterOn = chercher_sommet(g,1);
  int index = 0;
  while(toIterOn->sommet_suivant != NULL){
      printf (" %d \n", toIterOn->label);
      printf (" parent %d \n",  index);
      printf (" %d \n", toIterOn->parent[index]);
      index++;
  }

  return ;
}


// ======================================================================


int elementaire(pgraphe_t g, pchemin_t c){

  
  pchemin_t toRoam = c;
  psommet_t toIterOn = toRoam->debut;
  while(c != NULL){

      while(toRoam != NULL){
          if(toIterOn == toRoam->fin)
            return 0;
          toRoam = toRoam->chemin_suivant;
      }
      c = c->chemin_suivant;
      toRoam = c;
      toIterOn = c->debut;
  }
  return 1;
}

int simple(pgraphe_t g, pchemin_t c){

  parc_t toIterOn = c->arc_actuel;
  pchemin_t toRoam = c->chemin_suivant;
  while(c != NULL){
      while(toRoam != NULL){
          if(toIterOn == toRoam->arc_actuel)
            return 0;
          toRoam = toRoam->chemin_suivant;
      }
      c = c->chemin_suivant;
      if(c!=NULL){
        toRoam = c->chemin_suivant;
        toIterOn = c->arc_actuel;
      }
  }
  return 1;
}

int eulerien(pgraphe_t g, pchemin_t c){

  if(!simple(g,c))
    return 0;
  pchemin_t toIterOn = c;
  psommet_t toCheck = chercher_sommet(g,1);
  parc_t toRoam = NULL;
  int boole = 0;
  while(toCheck != NULL){
    toRoam = toCheck->liste_arcs;
    while(toRoam != NULL){
      while(toIterOn != NULL){
        if((toRoam->dest == toIterOn->fin) && (toCheck == toIterOn->debut)){
          boole++;
        }  
        toIterOn = toIterOn->chemin_suivant;
      }
      if (boole == 0)
        return 0;
      toIterOn = c;
      boole = 0;
      toRoam = toRoam -> arc_suivant;
    }
    toCheck = toCheck->sommet_suivant;
  }

  return 1;

}

int hamiltonien(pgraphe_t g, pchemin_t c){

  if(!elementaire(g,c))
    return 0;
  pchemin_t toIterOn = c;
  psommet_t toCheck = chercher_sommet(g,1);
  int occurences = 0;
  while(toCheck != NULL){
      if(toCheck == toIterOn->debut )
        occurences++;
      while(toIterOn != NULL){
        if(toCheck == toIterOn->fin){
          occurences++;
        }  
        toIterOn = toIterOn->chemin_suivant;
      }
      if (occurences != 1)
        return 0;
      toIterOn = c;
      occurences = 0;
      toCheck = toCheck->sommet_suivant;
  }

  return 1;
}

int graphe_eulerien(pgraphe_t g){

  psommet_t toCheck = chercher_sommet(g,1);
  int nbChemins = 0;
  while(toCheck != NULL){
      nbChemins = degre_sortant_sommet(g, toCheck) + degre_entrant_sommet (g, toCheck);
      if((nbChemins % 2) != 0){
        return 0;
      }
      toCheck = toCheck->sommet_suivant;
  }
  return 1;
}

int graphe_hamiltonien(pgraphe_t g){


  return 0;
}

int distance(pgraphe_t g, int x, int y){

  if(x == y)
    return 0;
  psommet_t toCheck = chercher_sommet(g,x);
  int distance = 0;
  parc_t listeChemin = toCheck->liste_arcs ;
  
  while(listeChemin != NULL){

  }

  return 1;
}

int excentricite(pgraphe_t g, int n){
  
  psommet_t toCheck = chercher_sommet(g,n);
  psommet_t toCompare = chercher_sommet(g,n);
  int max = 0;
  while(toCheck != NULL){
      if(distance(g, toCheck->label, toCompare->label) > max )
        max = distance(g, toCheck->label, toCompare->label);
      toCheck = toCheck->sommet_suivant;
  }
  return max;
}

int diametre(pgraphe_t g){

  psommet_t toCheck = chercher_sommet(g,1);
  int max = 0;
  while(toCheck != NULL){
      if(excentricite(g, toCheck->label) > max )
        max = excentricite(g, toCheck->label);
      toCheck = toCheck->sommet_suivant;
  }
  return max;
}

// ======================================================================




int degre_sortant_sommet (pgraphe_t g, psommet_t s)
{
  psommet_t toCheck = chercher_sommet(g,s->label);
  parc_t toCount = toCheck->liste_arcs;
  int res = 0;
  while(toCount!= NULL){
      res++;
      toCount = toCount->arc_suivant; 
  }
  return res;

}

int degre_entrant_sommet (pgraphe_t g, psommet_t s)
{
  psommet_t toCheck = chercher_sommet(g,s->label);
  parc_t toCount = toCheck->liste_arcs;
  int res = 0;
  while(toCheck!= NULL){
      res++;
      while(toCount!= NULL){
        if(toCount->dest->label == s->label){
          res++;
        }
        toCount = toCount->arc_suivant;
      }
      toCheck = toCheck->sommet_suivant;
      if(toCheck!= NULL)
        toCount = toCheck->liste_arcs;
  }
  return res;
}

int degre_maximal_graphe (pgraphe_t g)
{
  /*
    Max des degres des sommets du graphe g
  */

  return 0 ;
}


int degre_minimal_graphe (pgraphe_t g)
{
  /*
    Min des degres des sommets du graphe g
  */

  return 0 ;
}


int independant (pgraphe_t g)
{
  /* Les aretes du graphe n'ont pas de sommet en commun */

  return 0 ;
}



int complet (pgraphe_t g)
{
  /* Toutes les paires de sommet du graphe sont jointes par un arc */

  return 0 ;
}

int regulier (pgraphe_t g)
{
  /* 
     graphe regulier: tous les sommets ont le meme degre
     g est le ponteur vers le premier sommet du graphe
     renvoie 1 si le graphe est régulier, 0 sinon
  */

  return 0 ;
}




/*
  placer les fonctions de l'examen 2017 juste apres
*/
